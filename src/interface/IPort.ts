import IImageItem from "@/interface/IImageItem";

export default interface IPort extends IImageItem {
    port: number;
}
