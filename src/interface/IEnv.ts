import IImageItem from "@/interface/IImageItem";

export default interface IEnv extends IImageItem {
    env: number;
}
