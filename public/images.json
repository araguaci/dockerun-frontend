{
  "version": "2.1.0",
  "images": [
    {
      "name": "mysql",
      "commonName": "MySQL",
      "description": "MySQL is a widely used, open-source relational database management system (RDBMS)",
      "keywords": ["database", "mysql", "relational", "db"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/_/mysql",
      "color": "#F79518",
      "envs": [
        {
          "env": "MYSQL_ROOT_PASSWORD",
          "description": "Password for root account",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MYSQL_DATABASE",
          "description": "Creates a database with the provided name",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MYSQL_USER",
          "description": "This user will be granted superuser permissions for the database specified by the MYSQL_DATABASE variable. Use in conjunction with MYSQL_Password",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MYSQL_PASSWORD",
          "description": "Password for MYSQL_USER",
          "versions": ["latest"],
          "type": "text"
        }
      ],
      "ports": [
        {
          "port": 3306,
          "description": "Port used to connect to MySQL instance",
          "versions": ["latest"]
        }
      ],
      "volumes":  [
        {
          "volume": "/var/lib/mysql",
          "description": "Where MySQL stores databases data like tables",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "mariadb",
      "commonName": "MariaDB",
      "description": "MariaDB is a community-developed fork of MySQL intended to remain free under the GNU GPL",
      "keywords": ["database", "mariadb", "relational", "db"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/_/mariadb",
      "color": "#C49A6C",
      "envs": [
        {
          "env": "MYSQL_ROOT_PASSWORD",
          "description": "Password for root account",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MYSQL_DATABASE",
          "description": "Creates a database with the provided name",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MYSQL_USER",
          "description": "This user will be granted superuser permissions for the database specified by the MYSQL_DATABASE variable. Use in conjunction with MYSQL_Password",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MYSQL_PASSWORD",
          "description": "Password for MYSQL_USER",
          "versions": ["latest"],
          "type": "text"
        }
      ],
      "ports": [
        {
          "port": 3306,
          "description": "Port used to connect to MariaDB instance",
          "versions": ["latest"]
        }
      ],
      "volumes":  [
        {
          "volume": "/var/lib/mysql",
          "description": "Where MariaDB stores databases data like tables",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "dpage/pgadmin4",
      "commonName": "PgAdmin",
      "description": "pgAdmin is the most popular and feature rich Open Source administration and development platform for PostgreSQL",
      "keywords": ["database", "postgres", "postgresql", "pgadmin", "tool"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/r/dpage/pgadmin4/",
      "color": "#336791",
      "envs": [
        {
          "env": "PGADMIN_DEFAULT_EMAIL",
          "description": "Login email for web interface",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "PGADMIN_DEFAULT_PASSWORD",
          "description": "Login password for web interface",
          "versions": ["latest"],
          "type": "text"
        }
      ],
      "ports": [
        {
          "port": 80,
          "description": "Port used to connect via web interface (HTTP)",
          "versions": ["latest"]
        },
        {
          "port": 443,
          "description": "Port used to connect via web interface (HTTPS)",
          "versions": ["latest"]
        }
      ],
      "volumes":  [
        {
          "volume": "/var/lib/pgadmin",
          "description": "This is the working directory in which pgAdmin stores session data, user files, configuration files, and it's configuration database",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "phpmyadmin/phpmyadmin",
      "commonName": "phpMyAdmin",
      "description": "A web interface for MySQL and MariaDB",
      "keywords": ["database", "mysql", "mariadb", "phpmyadmin", "tool"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/r/phpmyadmin/phpmyadmin",
      "color": "#6C78AF",
      "ports": [
        {
          "port": 80,
          "description": "Port used to connect via web interface (HTTP)",
          "versions": ["latest"]
        }
      ],
      "links":  [
        {
          "link": "db",
          "description": "Container name of a running MySQL or MariaDB container",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "postgres",
      "commonName": "Postgres",
      "description": "The PostgreSQL object-relational database system provides reliability and data integrity",
      "keywords": ["database", "postgres", "postgresql", "relational", "db"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/_/postgres",
      "color": "#336791",
      "envs": [
        {
          "env": "POSTGRES_USER",
          "description": "Create the specified user with superuser power and a database with the same name. If it is not specified, then the default user of postgres will be used",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "POSTGRES_PASSWORD",
          "description": "Superuser password for PostgreSQL. Must NOT be empty.",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "POSTGRES_DB",
          "description": "Name for the default database. If it is not specified, then the value of POSTGRES_USER will be used",
          "versions": ["latest"],
          "type": "text"
        }
      ],
      "ports": [
        {
          "port": 5432,
          "description": "Port used to connect to Postgres instance",
          "versions": ["latest"]
        }
      ],
      "volumes":  [
        {
          "volume": "/var/lib/postgresql/data",
          "description": "Where Postgres stores databases data like tables",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "httpd",
      "commonName": "httpd",
      "description": "A Web server application notable for playing a key role in the initial growth of the World Wide Web",
      "keywords": ["web", "http", "httpd", "apache", "www"],
      "versions": ["latest", "alpine"],
      "link": "https://hub.docker.com/_/httpd",
      "color": "#C51C4D",
      "ports": [
        {
          "port": 80,
          "description": "Port used to connect via non-secure connection (HTTP)",
          "versions": ["latest", "alpine"]
        },
        {
          "port": 443,
          "description": "Port used to connect via secure connection (HTTPS)",
          "versions": ["latest", "alpine"]
        }
      ],
      "volumes":  [
        {
          "volume": "/usr/local/apache2/htdocs/",
          "description": "Web server content",
          "versions": ["latest", "alpine"]
        }
      ]
    },
    {
      "name": "nginx",
      "commonName": "nginx",
      "description": "Nginx is an open source reverse proxy server for HTTP, HTTPS, SMTP, POP3, IMAP protocols, load balancer, HTTP cache, and a web server",
      "keywords": ["web", "http", "nginx", "www"],
      "versions": ["latest", "alpine"],
      "link": "https://hub.docker.com/_/nginx",
      "color": "#00A652",
      "ports": [
        {
          "port": 80,
          "description": "Port used to connect via non-secure connection (HTTP)",
          "versions": ["latest", "alpine"]
        },
        {
          "port": 443,
          "description": "Port used to connect via secure connection (HTTPS)",
          "versions": ["latest", "alpine"]
        }
      ],
      "volumes":  [
        {
          "volume": "/usr/share/nginx/html/",
          "description": "Web server content",
          "versions": ["latest", "alpine"]
        }
      ]
    },
    {
      "name": "mongo",
      "commonName": "MongoDB",
      "description": "MongoDB document databases provide high availability and easy scalability",
      "keywords": ["database", "nosql", "mongo", "mongodb", "non-relational", "document", "json", "db"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/_/mongo",
      "color": "#4CB03E",
      "envs": [
        {
          "env": "MONGO_INITDB_ROOT_USERNAME",
          "description": "Sets custom user's username for login",
          "versions": ["latest"],
          "type": "text"
        },
        {
          "env": "MONGO_INITDB_ROOT_PASSWORD",
          "description": "Sets custom user's password for login",
          "versions": ["latest"],
          "type": "text"
        }
      ],
      "ports": [
        {
          "port": 27017,
          "description": "Port used to connect to MongoDB instance",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "redis",
      "commonName": "Redis",
      "description": "Redis is an open source key-value store that functions as a data structure server",
      "keywords": ["database", "nosql", "redis", "memory", "key-value"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/_/redis",
      "color": "#CD291C",
      "ports": [
        {
          "port": 6379,
          "description": "Port used to connect to Redis instance",
          "versions": ["latest"]
        }
      ]
    },
    {
      "name": "nextcloud",
      "commonName": "NextCloud",
      "description": "A safe home for all your data",
      "keywords": ["nextcloud", "cloud"],
      "versions": ["latest", "stable", "production"],
      "link": "https://hub.docker.com/_/nextcloud",
      "color": "#0A92DC",
      "ports": [
        {
          "port": 80,
          "description": "Port used to connect via non-secure connection (HTTP)",
          "versions": ["latest", "stable", "production"]
        },
        {
          "port": 443,
          "description": "Port used to connect via secure connection (HTTPS)",
          "versions": ["latest", "stable", "production"]
        }
      ],
      "volumes":  [
        {
          "volume": "/var/www/html/",
          "description": "Where all Nextcloud data lives",
          "versions": ["latest", "stable", "production"]
        }
      ],
      "links":  [
        {
          "link": "mysql",
          "description": "Container name of a running MySQL or MariaDB container",
          "versions": ["latest", "stable", "production"]
        }
      ]
    },
    {
      "name": "fsorge/dockerun",
      "commonName": "Dockerun",
      "description": "Start new containers on the fly with ease",
      "keywords": ["docker", "dockerun", "tool"],
      "versions": ["latest"],
      "link": "https://hub.docker.com/r/fsorge/dockerun",
      "color": "#1F97EE",
      "ports": [
        {
          "port": 80,
          "description": "Port used to connect via non-secure connection (HTTP)",
          "versions": ["latest"]
        }
      ]
    }
  ]
}
